import numpy as np
import pandas as pd
import datetime as dt

import boto.s3
import io
import os
import math
import csv
import ast
import psycopg2
import requests

from filechunkio import FileChunkIO
from sqlalchemy import create_engine, Table, Column, String, MetaData, Float, Numeric, DateTime, Float, Integer, Numeric, select

#helpers
def daterange(start_date, end_date):
	for n in range(int((end_date - start_date).days) + 1):
		yield start_date + dt.timedelta(n)

#connect to redshift
print('connecting to redshift..')
rs_conn = psycopg2.connect(
	host='rh1-data.cgptfmcyizib.us-west-2.redshift.amazonaws.com',
	user='admin',
	port=5439,
	password='4AdaPT118899-DAPt4',
	dbname='devimport')

curs = rs_conn.cursor()
print('........... connected...')

#fetch timestamp of last record
quer = """select MAX(dt_loc)
	from rh1office_weather
	limit 1;"""
curs.execute(quer)
last = curs.fetchone()[0]

#script parameters
wsid = 'KCASANFR940'
start = last.date()
end = dt.date.today()

#final dataframe
df = pd.DataFrame(columns=('Time','DateUTC<br>','DewpointF','Humidity','TemperatureF','WindSpeedMPH','WindSpeedGustMPH','WindDirectionDegrees','HourlyPrecipIn','dailyrainin','SolarRadiationWatts/m^2'))

#loop days within range
for date in daterange(start,end):
	date = str(date.strftime('%Y-%m-%d'))

	#extract elements
	yyyy = date[:4]
	mm =   date[:7][5:]
	dd =   date[-2:]

	#log function nav
	print('scraping day: ' + date)

	#send request
	url = 'https://www.wunderground.com/weatherstation/WXDailyHistory.asp?ID=' + wsid + '&day=' + dd + "&month="+ mm + "&year=" + yyyy + '&graphspan=day&format=1'
	r = requests.get(url).content
	
	#append response to dataframe
	d = pd.read_csv(io.StringIO(r.decode('utf-8')), index_col=False)
	d = d.dropna(0, thresh=3)
	
	#for first date, omit records earlier than last update
	if str(date) == str(start):

		for index, row in d.iterrows():
			if dt.datetime.strptime(row['Time'],'%Y-%m-%d %H:%M:%S') > last:

				#small dataframe
				sd = pd.DataFrame()

				#append small df to final df
				sd = d[d['Time'] == row['Time']]
				df = pd.concat([df,sd])

	#for other dates, append full response to final dataframe
	else:

		#append full response to final df
		df = pd.concat([df,d])

if df.shape[0] > 1:

	print('saving data to csv......')
	#format complete dataframe
	df = df[['Time','DateUTC<br>','DewpointF','Humidity','TemperatureF','WindSpeedMPH','WindSpeedGustMPH','WindDirectionDegrees','HourlyPrecipIn','dailyrainin','SolarRadiationWatts/m^2']] #keep column names as is in case WU column order changes
	df.columns = ['dt_loc','dt_utc','dewpt_f','relhum_pct','temp_f','windspd_mph','gustspd_mph','winddir_deg','precip_inchesperhr','precip_inchesaccumulated','solrad_wpms'] #change column names
	df.drop_duplicates(subset=['dt_loc'], inplace=True) #drop duplicates

	#save temp csv to local folder
	df.to_csv('temp.csv',header=True, index=False)

	print('uploading data to S3....')
	#connect to S3
	aws_acckey = 'AKIAILTPHWSDQAUKCLVQ'
	aws_secret = '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'
	s3_conn = boto.connect_s3(aws_acckey, aws_secret)
	s3_buck = s3_conn.get_bucket('rh1-extract')

	#upload csv in chunks
	mp = s3_buck.initiate_multipart_upload('rh1office/weatherstation/temp.csv')
	ss = os.stat('temp.csv').st_size #source size
	cs = 10485760 #chunk size
	cc = int(math.ceil(ss / float(cs))) #chunk count
	for i in range(cc):
	    offset = cs * i
	    bytes = min(cs, ss - offset)
	    print('uploading part: ' + str(i + 1) + ' of ' + str(cc))
	    with FileChunkIO('temp.csv', 'r', offset=offset, bytes=bytes) as fp:
	        mp.upload_part_from_file(fp, part_num=i + 1)
	mp.complete_upload()

	#remove temp csv from local folder
	os.remove('temp.csv')

	print('appending ' + str(df.shape[0] - 1) + ' records...')
	sql = """copy rh1office_weather (dt_loc,dt_utc,dewpt_f,relhum_pct,temp_f,windspd_mph,gustspd_mph,winddir_deg,precip_inchesperhr,precip_inchesaccumulated,solrad_wpms)
	    from 's3://rh1-extract/rh1office/weatherstation/temp.csv'
	    access_key_id 'AKIAILTPHWSDQAUKCLVQ'
	    secret_access_key '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'
	    region 'us-west-2'
	    acceptanydate
	    ignoreheader 1
	    null as 'NA'
	    removequotes
	    delimiter ',';"""
	curs.execute(sql)
	rs_conn.commit()
	print('..... update complete...')

	#close s3 connection
	s3_conn.close()

else:
	print('..... no new data yet...')

#close redshift connection
rs_conn.close()